/* 
 * File:   main.c
 * Author: abdullah kahraman
 *
 * Created on 24 Haziran 2013 Pazartesi, 09:37
 */
#include <xc.h> // Include the compiler's header file.
//This definition is needed for using __delay(int x):
#define _XTAL_FREQ 4000000 // We are using the 4MHz Internal Oscillator.

/******* Configuration Word ********/
__CONFIG(FOSC_INTRCIO & WDTE_OFF & PWRTE_ON & MCLRE_OFF & BOREN_ON & CP_ON & CPD_ON);
//INTOSC oscillator: I/O function on GP4/OSC2/CLKOUT pin, I/O function on GP5/OSC1/CLKIN
// WDT disabled
// PWRT enabled
// GP3/MCLR pin function is digital input, MCLR internally tied to VDD
// Brown-out Detect Enabled
// Program Memory code protection is enabled
// Data memory code protection is enabled
/**** End of Configuration Word ****/

/****** Input Output Definitions *******/
#define button_inc          GPIObits.GP2    // A button is connected here which increments period
#define TRIS_button_inc     TRISIO2         // Tri-state control bit
#define button_dec          GPIObits.GP3    // A button is connected here which decreases period
#define TRIS_button_dec     TRISIO3         // Tri-state control bit
#define LED_1               GPIObits.GP5    // An LED is connected to this pin
#define TRIS_LED_1          TRISIO5         // Tri-state control bit
#define LED_2               GPIObits.GP0    // An LED is connected to this pin
#define TRIS_LED_2          TRISIO0         // Tri-state control bit
#define sensor              GPIObits.GP1    // There is a sensor connected to this pin.
#define TRIS_sensor         TRISIO1         // Tri-state control bit
#define pulse               GPIObits.GP4    // Pulse output is given to driver
#define TRIS_pulse          TRISIO4         // Tri-state control bit
/*** End Of Input Output Definitions ***/

#define ON_time 300
#define BLANKING_TIME 100
#define BUTTON_TIMEOUT 5
#define DELAY_MAX_LIMIT 5000
#define DELAY_MIN_LIMIT 600

bit LED_toggler;
unsigned int counter = 0;
unsigned int delay = 600; // Delay in milliseconds.

void check_buttons(unsigned char delay_en)
{
    if (!button_inc)
    {
        if (delay_en == 1)
            __delay_ms(BUTTON_TIMEOUT);
        if (!button_inc)
        {
            if (delay < DELAY_MAX_LIMIT)
            {
                delay++;
            }
        }
    }
    else if (!button_dec)
    {
        if (delay_en == 1)
            __delay_ms(BUTTON_TIMEOUT);
        if (!button_dec)
        {
            if (delay > DELAY_MIN_LIMIT)
            {
                delay--;
                if (counter > delay) counter = delay;
            }
        }
    }
}

void main()
{
    CMCONbits.CM = 0x07; // CM2:CM0: Comparator Mode bits: 111 = Comparator OFF
    WPU = 0;
    WPUbits.WPU2 = 1;
    OPTION_REGbits.nGPPU = 0;
    TRIS_button_inc = 1; // Set the port as input
    TRIS_button_dec = 1; // Set the port as input
    TRIS_sensor = 1; // Set the port as input

    TRIS_LED_1 = 0; // Set the port as output
    LED_1 = 0; // and clear it.
    TRIS_LED_2 = 0; // Set the port as output
    LED_2 = 0; // and clear it.
    TRIS_pulse = 0; // Set the port as output
    pulse = 0; // and clear it.

    while (1)
    {
        LED_toggler = !LED_toggler;
        if (LED_toggler)
        {
            LED_1 = 1;
            LED_2 = 0;
        }
        else
        {
            LED_1 = 0;
            LED_2 = 1;
        }
        while (sensor == 1)
        {
            check_buttons(1);
        }

        __delay_ms(BLANKING_TIME);

        counter = ON_time;
        while (counter > 0)
        {
            pulse = 1;
            __delay_us(500);
            pulse = 0;
            __delay_us(500);
            counter--;
        }

        pulse = 0;
        counter = delay;
        while (counter > 0)
        {
            __delay_ms(1);
            check_buttons(0);
            counter--;
        }
    }
}
